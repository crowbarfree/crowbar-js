call :build-forms
call :build-contents
call :build-wbt
exit

rem -----------------------------------------------------------
:build-forms
del built\forms.js
node optimizers\r.js -o conf\build-debug-forms.js
exit /B

rem -----------------------------------------------------------
:build-contents
del built\contents.js
node optimizers\r.js -o conf\build-debug-contents.js
exit /B

rem -----------------------------------------------------------
:build-wbt
del built\wbt.js
node optimizers\r.js -o conf\build-debug-wbt.js
exit /B
