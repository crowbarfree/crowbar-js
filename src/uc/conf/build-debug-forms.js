({
    baseUrl: "..",
    include: ['js/forms'],
    name: "optimizers/almond",
    out: "..\\built\\forms.js",
    wrap: true,
    map:
    {
      '*':
      {
        tpl: 'js/libs/tpl',
        txt: 'js/libs/txt'
      }
    }
})