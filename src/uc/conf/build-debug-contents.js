({
    baseUrl: "..",
    include: ['js/contents'],
    name: "optimizers/almond",
    out: "..\\built\\contents.js",
    wrap: true,
    map:
    {
      '*':
      {
        tpl: 'js/libs/tpl',
        txt: 'js/libs/txt'
      }
    }
})