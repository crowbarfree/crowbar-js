﻿define(['forms/base/controller'], function (BaseFormController)
{
	return function ()
	{
		return BaseFormController();
	}
});
