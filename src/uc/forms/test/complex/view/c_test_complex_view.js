﻿define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/test/complex/view/e_test_complex_view.html'
],
function (c_nailed, tpl)
{
	return function ()
	{
		var controller = c_nailed(tpl);
		return controller;
	}
});