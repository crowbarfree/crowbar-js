﻿define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/test/collection/simplest/e_test_collection0.html'
],
function (c_nailed, tpl)
{
	return function ()
	{
		var controller = c_nailed(tpl);
		return controller;
	}
});