﻿define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/test/nailed/simple/e_simple.html'
],
function (c_nailed, tpl)
{
	return function ()
	{
		var controller = c_nailed(tpl);

		return controller;
	}
});