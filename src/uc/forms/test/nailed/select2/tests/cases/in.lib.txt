
start_store_lines_as select2_fields_1

  js wbt.SetModelFieldValue("first",  "варианта 2");
  js wbt.CheckModelFieldValue("first",{"id":"вариант 2","text":"текст для варианта 2","element":[{}],"disabled":false,"locked":false});

  js wbt.SetModelFieldValue("second", "варианта 3");
  js wbt.CheckModelFieldValue("second",{"id": "вариант 3","text": "текст для варианта 3"});

  js wbt.SetModelFieldValue("third", "варианта 11");
  js wbt.CheckModelFieldValue("third",{"id": "вариант 11","text": "текст для варианта 11"});

stop_store_lines


start_store_lines_as select2_fields_2

  js wbt.SetModelFieldValue("first",  "варианта 3");
  js wbt.CheckModelFieldValue("first",{"id":"вариант 3","text":"текст для варианта 3","element":[{}],"disabled":false,"locked":false});

  js wbt.SetModelFieldValue("second", "варианта 1");
  js wbt.CheckModelFieldValue("second",{"id": "вариант 1","text": "текст для варианта 1"});

  js wbt.SetModelFieldValue("third", "варианта 21");
  js wbt.CheckModelFieldValue("third",{"id": "вариант 21","text": "текст для варианта 21"});

stop_store_lines

