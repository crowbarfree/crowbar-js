﻿define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/test/nailed/select2/e_select2.html'
],
function (c_nailed, tpl)
{
	return function ()
	{
		var select_variants = [
			  { id: 'вариант 1', text: 'текст для варианта 1' }
			, { id: 'вариант 2', text: 'текст для варианта 2' }
			, { id: 'вариант 3', text: 'текст для варианта 3' }
			, { id: 'вариант 4', text: 'текст для варианта 4' }
			, { id: 'вариант 5', text: 'текст для варианта 5' }
			, { id: 'вариант 6', text: 'текст для варианта 6' }
			, { id: 'вариант 7', text: 'текст для варианта 7' }
			, { id: 'вариант 8', text: 'текст для варианта 8' }
			, { id: 'вариант 9', text: 'текст для варианта 9' }
			, { id: 'вариант 10', text: 'текст для варианта 10' }
			, { id: 'вариант 11', text: 'текст для варианта 11' }
			, { id: 'вариант 12', text: 'текст для варианта 12' }
			, { id: 'вариант 13', text: 'текст для варианта 13' }
			, { id: 'вариант 14', text: 'текст для варианта 14' }
			, { id: 'вариант 15', text: 'текст для варианта 15' }
			, { id: 'вариант 16', text: 'текст для варианта 16' }
			, { id: 'вариант 17', text: 'текст для варианта 17' }
			, { id: 'вариант 18', text: 'текст для варианта 18' }
			, { id: 'вариант 19', text: 'текст для варианта 19' }
			, { id: 'вариант 20', text: 'текст для варианта 20' }
			, { id: 'вариант 21', text: 'текст для варианта 21' }
			, { id: 'вариант 22', text: 'текст для варианта 22' }
			, { id: 'вариант 23', text: 'текст для варианта 23' }
		];

		var options = {
			field_spec:
				{
					second: { data: select_variants }
					, third:
						{
							query: function (cb)
							{
								var data = { results: [] };
								for (var i = 0; i < select_variants.length && data.results.length<2; i++)
								{
									var variant = select_variants[i];
									if (-1 != variant.text.indexOf(cb.term))
										data.results.push(variant);
								}
								cb.callback(data);
							}
						}
				}
		};

		var controller = c_nailed(tpl, options);

		return controller;
	}
});