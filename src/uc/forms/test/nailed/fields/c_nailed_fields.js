﻿define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/test/nailed/fields/e_nailed_fields.html'
],
function (c_nailed, tpl)
{
	return function ()
	{
		var controller = c_nailed(tpl);
		return controller;
	}
});