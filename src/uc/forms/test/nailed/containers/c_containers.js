﻿define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/test/nailed/containers/e_containers.html'
	, 'forms/test/nailed/simple/c_simple'
],
function (c_nailed, tpl, c_simple)
{
	return function ()
	{
		var options= {
			field_spec:
			{
				  Сам: { controller: c_simple }
				, Шеф: { controller: c_simple }
			}
		};
		var controller = c_nailed(tpl, options);
		return controller;
	}
});