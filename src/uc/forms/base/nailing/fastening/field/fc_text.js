﻿define([
	  'forms/base/nailing/fastening/fc_abstract'
	, 'forms/base/nailing/fastening/h_fastening_clip'
]
, function (fc_abstract, h_fastening_clip)
{
	var fc_text = fc_abstract();

	fc_text.match = function (adom_item, tag_name, fc_type)
	{
		return 'text' == fc_type;
	}

	fc_text.load_from_model = function (model, model_selector, dom_item, fc_data)
	{
		model_selector = dom_item.attr('model-selector');

		var value = h_fastening_clip.get_model_field_value(model, model_selector);
		var fixed_value = (!value || null == value) ? '' : value.toString().replace(/\"/g, '&quot;');
		dom_item.text(fixed_value);

		return fc_data;
	}

	fc_text.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.text_field = function (model_selector)
		{
			var res = '<span ';
			res += this.fastening_attrs(model_selector, 'text');
			res += '>';

			var value = this.value();
			var fixed_value = (!value || null == value) ? '' : value.toString().replace(/\"/g, '&quot;');
			res += fixed_value;

			return res;
		}

		_template_argument.text_field_end = function ()
		{
			return '</span>';
		}
	}

	return fc_text;
});