﻿define([
	  'forms/base/nailing/fastening/fc_abstract'
	, 'forms/base/nailing/fastening/h_fastening_clip'
]
, function (fc_abstract, h_fastening_clip)
{
	var fc_select2 = fc_abstract();

	fc_select2.match = function (dom_item, tag_name, fc_type)
	{
		return 'select2' == fc_type;
	}

	fc_select2.render = function (options, model, model_selector, adom_item)
	{
		var dom_item = $(adom_item);
		dom_item.select2(options);
		if (model)
		{
			var value = h_fastening_clip.get_model_field_value(model, model_selector);
			dom_item.select2('data', value);
		}
	}

	fc_select2.load_from_model = function (model, model_selector, dom_item)
	{
		var value = h_fastening_clip.get_model_field_value(model, model_selector);
		$(dom_item).select2('data',value);
	}

	fc_select2.save_to_model = function (model, model_selector, dom_item)
	{
		var value = $(dom_item).select2('data');
		return h_fastening_clip.set_model_field_value(model, model_selector, value);
	}

	fc_select2.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.select2_attrs = function (model_selector)
		{
			return this.fastening_attrs(model_selector, 'select2');
		}
	}

	return fc_select2;
});