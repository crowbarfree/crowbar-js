﻿define([
	  'forms/base/nailing/fastening/fc_abstract'
	, 'forms/base/nailing/fastening/extra/fc_typeahead'
	, 'forms/base/nailing/fastening/field/input/fc_input_checkbox'
	, 'forms/base/nailing/fastening/field/input/fc_input_radio'
	, 'forms/base/nailing/fastening/field/input/fc_input_date'
	, 'forms/base/nailing/fastening/field/input/fc_input_text'
	, 'forms/base/nailing/fastening/extra/fc_select2'
	, 'forms/base/nailing/fastening/field/fc_select'
	, 'forms/base/nailing/fastening/field/fc_textarea'
	, 'forms/base/nailing/fastening/control/fc_control'
	, 'forms/base/nailing/fastening/control/fc_fields'
	, 'forms/base/nailing/fastening/extra/fc_tabs'
	, 'forms/base/nailing/fastening/control/fc_a_modal'
	, 'forms/base/nailing/fastening/fc_with'
	, 'forms/base/nailing/fastening/field/fc_text'
	, 'forms/base/nailing/fastening/array/fc_empty_array'
	, 'forms/base/nailing/fastening/array/fc_not_empty_array'
	, 'forms/base/nailing/fastening/array/fc_array_position'
	, 'forms/base/nailing/fastening/array/fc_array_item'
]
, function (w_abstract)
{
	var fastenings = arguments;
	var w_abstract_instance = w_abstract();
	var h_fastening_clips = w_abstract_instance.h_fastening_clips;

	h_fastening_clips.find_methods_for = function (adom_item)
	{
		var dom_item = $(adom_item);
		var tag_name = dom_item.prop("tagName").toUpperCase();
		var fc_type = dom_item.attr('fc-type');
		for (var i = 1; i < fastenings.length; i++)
		{
			var fastening = fastenings[i];

			if (fastening.match(dom_item, tag_name, fc_type))
				return fastening;
		}
		return w_abstract();
	}

	h_fastening_clips.add_template_argument_methods = function (nailed)
	{
		for (var i = 1; i < fastenings.length; i++)
		{
			var fastening = fastenings[i];
			fastening.add_template_argument_methods(nailed);
		}
	}

	return h_fastening_clips;
});