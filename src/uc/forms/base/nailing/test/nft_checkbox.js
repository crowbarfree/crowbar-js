﻿define([
	  'forms/base/nailing/test/nft_abstract'
]
, function (nft_abstract)
{
	var nailed_checkbox_tester = nft_abstract();

	nailed_checkbox_tester.match = function (dom_item, tag_name, fc_type)
	{
		return 'checkbox' == dom_item.attr('type');
	}

	nailed_checkbox_tester.check_value = function (dom_item, value)
	{
		var evalue = dom_item.attr('checked');
		if (value == evalue)
		{
			return null; // ' ok, value "' + value + '" is checked';
		}
		else
		{
			return ' value "' + evalue + '"! is WRONG!!!!!! should be "' + value + '"';
		}
	}

	nailed_checkbox_tester.set_value = function (dom_item, value)
	{
		dom_item.attr('checked', value).change();
		return 'set value "' + value + '"';
	}

	return nailed_checkbox_tester;
});