﻿define([
	  'forms/base/nailing/test/nft_abstract'
]
, function (nft_abstract)
{
	var nailed_typeahead_tester = nft_abstract();

	nailed_typeahead_tester.match = function (dom_item, tag_name, fc_type)
	{
		return 'typeahead' == fc_type;
	}

	nailed_typeahead_tester.check_value = function (dom_item, value, instead_of_set)
	{
		if (instead_of_set)
		{
			return null;
		}
		else
		{
			var evalue = dom_item.filter('.tt-input').val();
			if (value == evalue)
			{
				return null; // 'ok, value is "' + value + '"';
			}
			else
			{
				return ' value "' + evalue + '"! is WRONG!!!!!! should be "' + value + '"';
			}
		}
	}

	nailed_typeahead_tester.set_value = function (dom_item, value)
	{
		var on_render;
		on_render = function ()
		{
			dom_item.unbind('typeahead:render', on_render);
			var parent = dom_item.parent();
			var menu = parent.find('.tt-menu');
			var suggestion = menu.find('div.tt-suggestion:first-child');
			suggestion.mousedown().mouseup().click();
			dom_item.typeahead('close');
			app.wbt_long_process_result = ' selected text "' + value + '"';
		}
		dom_item.typeahead('val', '')
		dom_item.bind('typeahead:render', on_render);
		dom_item.typeahead('val', value);

		return "wait_long_process!";
	}

	return nailed_typeahead_tester;
});