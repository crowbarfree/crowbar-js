﻿define([
	  'forms/base/nailing/test/nft_abstract'
]
, function (nft_abstract)
{
	var default_nailed_field_tester = nft_abstract();

	default_nailed_field_tester.match = function (dom_item, tag_name, fc_type)
	{
		return true;
	}

	default_nailed_field_tester.check_value = function (dom_item, value)
	{
		var evalue= dom_item.val();
		if (value==evalue)
		{
			return null; // 'ok, value is "' + value + '"';
		}
		else
		{
			return ' value "' + evalue + '"! is WRONG!!!!!! should be "' + value + '"';
		}
	}

	default_nailed_field_tester.set_value = function (dom_item, value)
	{
		dom_item.val(value);
		dom_item.change();
		dom_item.trigger('focusout');
		return 'set value "' + value + '"';
	}

	return default_nailed_field_tester;
});