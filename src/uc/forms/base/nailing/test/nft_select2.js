﻿define([
	  'forms/base/nailing/test/nft_abstract'
]
, function (nft_abstract)
{
	var nailed_select2_tester = nft_abstract();

	nailed_select2_tester.match = function (dom_item, tag_name, fc_type)
	{
		return 'select2' == fc_type;
	}

	var Focus= function(dom_item)
	{
		var sel = '#' + dom_item.prev().attr('id');
		$(sel + ' .select2-focusser').focus();
		$(sel + ' .select2-choice').click().mousedown().mouseup();
		$(sel + ' .select2-focusser').focus();
		$(sel).select2('close');
	}

	nailed_select2_tester.check_value = function (dom_item, avalue, instead_of_set)
	{
		Focus(dom_item);
		if (instead_of_set)
		{
			return null;
		}
		else
		{
			var aevalue = dom_item.select2('data');

			var value = JSON.stringify(avalue);
			var evalue = JSON.stringify(aevalue);

			if (value == evalue)
			{
				return null; // 'ok, value is "' + value + '"';
			}
			else
			{
				return ' value "' + evalue + '"! is WRONG!!!!!! should be "' + value + '"';
			}
		}
	}

	nailed_select2_tester.set_value = function (dom_item, value)
	{
		var on_selected_on_text;
		on_selected_on_text = function ()
		{
			var labels = $('.select2-result-label');
			dom_item.unbind('select2-loaded', on_selected_on_text);
			if (1 > labels.length || -1 == $(labels[0]).text().indexOf(value))
			{
				dom_item.select2('close');
				app.wbt_long_process_result = 'can not find text "' + value + '" to select !!!!!!!!!';
			}
			else
			{
				$('#select2-drop ul li').first().mousedown().mouseup().click();
				$('.select2-input').val('').change().keydown().keyup();
				dom_item.select2('close');
				app.wbt_long_process_result = ' selected text "' + value + '"';
			}
		};
		var on_selected_on_focus;
		on_selected_on_focus = function ()
		{
			dom_item.unbind('select2-loaded', on_selected_on_focus);
			$('.select2-input').val('').change().keydown().keyup().change();
			dom_item.bind('select2-loaded', on_selected_on_text);
			$('.select2-input').val(value).change().keydown().keyup().change();
		};
		dom_item.bind('select2-loaded', on_selected_on_focus);

		Focus(dom_item);

		return "wait_long_process!";
	}

	return nailed_select2_tester;
});